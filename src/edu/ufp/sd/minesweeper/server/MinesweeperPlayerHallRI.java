package edu.ufp.sd.minesweeper.server;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashMap;

public interface MinesweeperPlayerHallRI extends Remote {

    public ArrayList<MinesweeperGameSubjectRI> createGame() throws RemoteException;
    public ArrayList<MinesweeperGameSubjectRI> listGames() throws RemoteException;
    //public MinesweeperGameSubjectRI joinGame(MinesweeperGameSubjectRI gameSubjectRI, Session playerSession) throws RemoteException;

}
