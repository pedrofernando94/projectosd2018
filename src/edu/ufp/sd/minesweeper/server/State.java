package edu.ufp.sd.minesweeper.server;

import java.io.Serializable;

public class State implements Serializable {

    public int x = -1; // Used on action = "CLICK" to determine position x
    public int y = -1; // Used on action = "CLICK" to determine position y

    public int[][] mineField = null; // The generated minefield to be used by everyone

    public String action = ""; // Describes what was done

    public String playerContext = null; // Nickname of player that made the play.

    public State() { }

}
