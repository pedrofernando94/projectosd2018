package edu.ufp.sd.minesweeper.server;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class MinesweeperSessionFactoryImpl extends UnicastRemoteObject implements MinesweeperSessionFactoryRI {

    protected BD bd;

    public MinesweeperSessionFactoryImpl(BD bd) throws RemoteException {
        super();
        this.bd = bd;
    }

    @Override
    public Session loginOrRegister(String nickname) throws RemoteException {
        for (Session s : this.bd.sessions) { // This may need to be commented for it to function properly.
            if (s.getNickname().equals(nickname)) {
                return s;
            }
        }
        Session newSession = new Session(nickname, this, new MinesweeperPlayerHallImpl(bd));
        this.bd.sessions.add(newSession);
        return newSession;
    }

    @Override
    public void logout(Session session) {
        bd.sessions.remove(session);
        // TODO: Detach observer from games using BD...
    }
}
