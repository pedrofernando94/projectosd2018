package edu.ufp.sd.minesweeper.server;

import edu.ufp.sd.minesweeper.client.MinesweeperGameObserverRI;
import edu.ufp.sd.minesweeper.client.minefield.jgui.MainFrame;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.HashMap;

public class MinesweeperGameSubjectImpl extends UnicastRemoteObject implements MinesweeperGameSubjectRI {

    private State state;

    protected ArrayList<MinesweeperGameObserverRI> observers;
    protected HashMap <String, Integer> points;
    private int playerTurn;
    protected ArrayList<State> historicGame ;
    protected boolean isGameStarted;


    protected MinesweeperGameSubjectImpl() throws RemoteException {
        super();
        this.state = new State();
        this.observers = new ArrayList<>();
        this.playerTurn = 0;
        this.points = new HashMap<String, Integer>();
        this.historicGame = new ArrayList<>();
    }

    @Override
    public void attach(MinesweeperGameObserverRI client) throws RemoteException {

        // Maximum of 4 players for each game (Trully this needs to be verified in another way.
        if(observers.size() <= 4) {
            this.observers.add(client);
            System.out.println("nickname : " + client.getPlayer().getNickname());

            // If the game has 2 or more players the state should be that the game started.
            this.isGameStarted = (this.observers.size() >= 2);
            State auxGame = new State();
            auxGame.action = "GAME_STATE_CHANGED";
            this.setState(auxGame);
        }
    }


    @Override
    public void detach(MinesweeperGameObserverRI client) throws RemoteException {
        this.observers.remove(client);
    }

    @Override
    public State getState() throws RemoteException {
        return this.state;
    }

    @Override
    public void setState(State s) throws RemoteException {
        // Continuously include the generated minefield in the state only if not a new game was called.
        if (!s.action.equals("NEW_GAME")) {
            if (s.mineField == null && this.state != null) {
                s.mineField = this.state.mineField;
            }
        }

        if(s.action.equals("NEW_GAME")) { // When someone pressed NEW:GAME
            historicGame = new ArrayList<>();
        }

        if (s.action.equals("CLICK")) { //TODO: Falta verificar se o clique é válido
            this.skipPlayerTurn();
            historicGame.add(s);
        }

        if(s.action.equals("PASS_TURN")) { // When someone skips his turn
            this.skipPlayerTurn();
        }

        this.state = s;

        this.notifyAllObservers();
    }

    public void notifyAllObservers() throws RemoteException {
        for (MinesweeperGameObserverRI observerRI : observers) {
            observerRI.update(this.state);
        }
    }

    /**
     * Gives the next player on the observers lists the turn.
     */
    public void skipPlayerTurn() {
        this.playerTurn = (playerTurn+1) % (observers.size());
        System.out.println(this.playerTurn);
    }

    /**
     * Checks if it is a givens player turn by nickname
     * @param player
     * @return
     * @throws RemoteException
     */
    @Override
    public boolean isPlayerTurn(String player) throws RemoteException {
        if(observers.get(this.playerTurn).getPlayer().getNickname().equals(player)) {
            return true;
        }
        return false;
    }

    /**
     * Atribui pontos a um jogador
     * @param player
     * @param point
     * @throws RemoteException
     */
    @Override
    public void setPoints (String player, Integer point) throws RemoteException
    {
        Integer lastPoints = this.points.putIfAbsent(player, point);
        if(lastPoints != null) {
            Integer newPoint = lastPoints + point;
            this.points.put(player, newPoint);
        }
        this.updatePoints();
    }

    /**
     * Notifica todos os observers que os pontos de cada jogador foram alterados
     * @throws RemoteException
     */
    public void updatePoints() throws RemoteException {
        for (MinesweeperGameObserverRI observerRI : observers) {
            observerRI.updatePoints(this.points);
        }
    }

    @Override
    public ArrayList<State> getHistory () throws RemoteException{
        return historicGame;
    }

    @Override
    public boolean isGameStarted() throws RemoteException{
        return isGameStarted;
    }

    @Override
    public boolean canEnterGame() throws RemoteException {
        return (observers.size() < 4);
    }


}
