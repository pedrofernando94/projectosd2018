package edu.ufp.sd.minesweeper.server;

import edu.ufp.sd.minesweeper.client.MinesweeperGameObserverRI;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;

public interface MinesweeperGameSubjectRI extends Remote {

    // Observer Design Pattern
    public void attach(MinesweeperGameObserverRI client) throws RemoteException;
    public void detach(MinesweeperGameObserverRI client) throws RemoteException;
    public State getState() throws RemoteException;
    public void setState(State s) throws RemoteException;

    public boolean isPlayerTurn(String player) throws RemoteException;
    public void setPoints (String player, Integer point) throws RemoteException;
    public ArrayList<State> getHistory () throws RemoteException;
    public boolean isGameStarted() throws RemoteException;
    public boolean canEnterGame() throws RemoteException;
}
