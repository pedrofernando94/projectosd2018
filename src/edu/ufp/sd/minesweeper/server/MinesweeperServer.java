package edu.ufp.sd.minesweeper.server;

import edu.ufp.sd.util.rmisetup.SetupContextRMI;

import java.rmi.RemoteException;
import java.rmi.registry.Registry;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MinesweeperServer {

    /**
     * Context for running a RMI Servant on a host
     */
    private SetupContextRMI contextRMI;

    /**
     * Remote interfaces
     */
    private MinesweeperSessionFactoryRI sessionFactoryRI;

    public static void main(String[] args) throws RemoteException {
        if (args != null && args.length < 3) {
            System.exit(-1);
        } else {
            MinesweeperServer minesweeperServerGame = new MinesweeperServer(args);
            minesweeperServerGame.rebindService();
        }
    }

    public MinesweeperServer(String args[]) throws RemoteException {
        try {
            String registryIP = args[0];
            String registryPort = args[1];
            String serviceName = args[2];
            contextRMI = new SetupContextRMI(this.getClass(), registryIP, registryPort, new String[]{serviceName});
        } catch (RemoteException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
        }
    }

    private void rebindService() {
        try {
            //Get proxy to rmiregistry
            Registry registry = contextRMI.getRegistry();
            //Bind service on rmiregistry and wait for calls
            if (registry != null) {
                //============ Create Servant ============
                BD bd = new BD();
                sessionFactoryRI = (MinesweeperSessionFactoryRI) new MinesweeperSessionFactoryImpl(bd);

                //Get service url (including servicename)
                String serviceUrl = contextRMI.getServicesUrl(0);
                Logger.getLogger(this.getClass().getName()).log(Level.INFO, "going to rebind service @ {0}", serviceUrl);

                registry.rebind(serviceUrl, sessionFactoryRI);

                Logger.getLogger(this.getClass().getName()).log(Level.INFO, "service bound and running. :)");
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.INFO, "registry not bound (check IPs). :(");
            }
        } catch (RemoteException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        }
    }

}
