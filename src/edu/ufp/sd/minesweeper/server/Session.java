package edu.ufp.sd.minesweeper.server;

import edu.ufp.sd.minesweeper.client.MinesweeperGameObserverRI;

import java.io.Serializable;

public class Session implements Serializable {

    private String nickname;
    private MinesweeperSessionFactoryRI sessionFactoryRI;
    private MinesweeperPlayerHallRI playerHallRI;

    public Session(String nickname, MinesweeperSessionFactoryRI sessionFactoryRI, MinesweeperPlayerHallRI playerHallRI) {
        this.nickname = nickname;
        this.sessionFactoryRI = sessionFactoryRI;
        this.playerHallRI = playerHallRI;
    }

    public String getNickname() {
        return nickname;
    }

    public MinesweeperSessionFactoryRI getSessionFactoryRI() {
        return sessionFactoryRI;
    }

    public MinesweeperPlayerHallRI getPlayerHallRI() {
        return playerHallRI;
    }

}
