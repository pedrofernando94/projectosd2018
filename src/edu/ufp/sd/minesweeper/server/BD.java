package edu.ufp.sd.minesweeper.server;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashMap;

public class BD {

    protected ArrayList<Session> sessions;
    protected ArrayList<MinesweeperGameSubjectRI> games;

    public BD() {
        this.sessions = new ArrayList<>();
        this.games = new ArrayList<>();
    }
}
