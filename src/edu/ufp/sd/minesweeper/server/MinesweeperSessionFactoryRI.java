package edu.ufp.sd.minesweeper.server;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface MinesweeperSessionFactoryRI extends Remote {

    public Session loginOrRegister(String nickname) throws RemoteException;
    public void logout(Session session) throws RemoteException;

}
