package edu.ufp.sd.minesweeper.server;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.HashMap;

public class MinesweeperPlayerHallImpl extends UnicastRemoteObject implements MinesweeperPlayerHallRI {

    protected BD bd;

    protected MinesweeperPlayerHallImpl(BD bd) throws RemoteException {
        super();
        this.bd = bd;
    }

    @Override
    public ArrayList<MinesweeperGameSubjectRI> createGame() throws RemoteException {
        bd.games.add(new MinesweeperGameSubjectImpl());
        return bd.games;
    }

    @Override
    public ArrayList<MinesweeperGameSubjectRI> listGames() throws RemoteException {
        return bd.games;
    }

    /*
    @Override
    public MinesweeperGameSubjectRI joinGame(MinesweeperGameSubjectRI gameSubjectRI, Session playerSession) throws RemoteException {
        return gameSubjectRI;
    }
    */
}
