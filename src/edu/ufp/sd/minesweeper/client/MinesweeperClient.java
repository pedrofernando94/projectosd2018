package edu.ufp.sd.minesweeper.client;

import edu.ufp.sd.minesweeper.client.gui.MinesweeperLoginDialog;
import edu.ufp.sd.minesweeper.client.minefield.jgui.MainFrame;
import edu.ufp.sd.minesweeper.client.minefield.jgui.MineSweeperMenuBar;
import edu.ufp.sd.minesweeper.client.models.GameModeManager;
import edu.ufp.sd.minesweeper.client.timers.SystemTimeUpdater;
import edu.ufp.sd.minesweeper.server.MinesweeperGameSubjectRI;
import edu.ufp.sd.minesweeper.server.MinesweeperSessionFactoryRI;
import edu.ufp.sd.util.rmisetup.SetupContextRMI;

import java.rmi.NotBoundException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.registry.Registry;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MinesweeperClient {

    /**
     * Context for connecting a RMI client to a RMI Servant
     */
    private SetupContextRMI contextRMI;

    /**
     * Remote interface that will hold the Servant proxy
     */
    private MinesweeperSessionFactoryRI sessionFactoryRI;

    public static void main(String[] args) {
        if (args != null && args.length < 2) {
            System.exit(-1);
        } else {
            MinesweeperClient minesweeperClient = new MinesweeperClient(args);
            //2. ============ Lookup service ============
            minesweeperClient.lookupService();
            //3. ============ Play with service ============
            minesweeperClient.playService();
        }
    }

    public MinesweeperClient(String args[]) {
        try {
            String registryIP = args[0];
            String registryPort = args[1];
            String serviceName = args[2];
            contextRMI = new edu.ufp.sd.util.rmisetup.SetupContextRMI(this.getClass(), registryIP, registryPort, new String[]{serviceName});
        } catch (RemoteException e) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, e);
        }
    }

    private Remote lookupService() {
        try {
            //Get proxy to rmiregistry
            Registry registry = contextRMI.getRegistry();
            //Lookup service on rmiregistry and wait for calls
            if (registry != null) {
                //Get service url (including servicename)
                String serviceUrl = contextRMI.getServicesUrl(0);
                Logger.getLogger(this.getClass().getName()).log(Level.INFO, "going to lookup service @ {0}", serviceUrl);

                //============ Get proxy to Minesweeper service ============
                sessionFactoryRI = (MinesweeperSessionFactoryRI) registry.lookup(serviceUrl);

            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.INFO, "registry not bound (check IPs). :(");
            }
        } catch (RemoteException | NotBoundException ex) {
            Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
        }
        return sessionFactoryRI;
    }

    private void playService() {
        MinesweeperLoginDialog dialog = new MinesweeperLoginDialog(sessionFactoryRI);
        dialog.pack();
        dialog.setVisible(true);
    }

}
