package edu.ufp.sd.minesweeper.client;

import edu.ufp.sd.minesweeper.client.minefield.jgui.MainFrame;
import edu.ufp.sd.minesweeper.client.minefield.jgui.MineSweeperStatusBar;
import edu.ufp.sd.minesweeper.server.MinesweeperGameSubjectRI;
import edu.ufp.sd.minesweeper.server.Session;
import edu.ufp.sd.minesweeper.server.State;


import javax.security.auth.Subject;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;

public class MinesweeperGameObserverImpl implements MinesweeperGameObserverRI {

    private MinesweeperGameSubjectRI subjectRI;

    private Session player;

    public MinesweeperGameObserverImpl(MinesweeperGameSubjectRI subjectRI, Session player) throws RemoteException {
        this.exportObject();
        this.subjectRI = subjectRI;
        this.player = player;
        this.subjectRI.attach(this);



    }

    private void exportObject() throws RemoteException {
        UnicastRemoteObject.exportObject(this, 0);
    }

    @Override
    public void update(State state) throws RemoteException {

        if (state.action == null || state.action.equals("")) {
            return;
        }

        if (state.action.equals("CLICK")) {
            if (state.x >= 0 && state.y >= 0) { // Se houve um clique válido.
                MainFrame.getInstance().mineFieldPanel.getBombButtons()[state.x][state.y].bombButtonPressed(state.playerContext);
            }
        }

        if (state.action.equals("NEW_GAME")) { // When New Game was asked.
            MainFrame.getInstance().startNewGame();
        }

        if (state.action.equals("GENERATE_MINEFIELD")) { // When a minefield was successfully generated
            MainFrame.getInstance().startNewGame();
        }

        if (state.action.equals("CLICK_HIST")) { // A click made only to update UI, does not affect game stats.
            MainFrame.getInstance().mineFieldPanel.getBombButtons()[state.x][state.y].bombButtonPressed(null);
        }

        if(state.action.equals(("GAME_STATE_CHANGED"))) { // Change the state of the game (tell if it started or not)
            MainFrame.getInstance().gameStarted(subjectRI.isGameStarted());
        }


        //if (state.action.equals("PASS_TURN")) { }

    }

    @Override
    public MinesweeperGameSubjectRI getSubjectRI() throws RemoteException {
        return subjectRI;
    }

    @Override
    public Session getPlayer() throws RemoteException {
        return player;
    }

    @Override
    public void updatePoints(HashMap<String, Integer> points) throws RemoteException {
        String infoText = "";
        for (String nick : points.keySet()) {
            infoText += nick + ": " + points.get(nick).toString() + "\n";
        }
        MineSweeperStatusBar.getInstance().infoPanel.setText(infoText);
    }



}
