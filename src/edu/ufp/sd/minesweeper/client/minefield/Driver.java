package edu.ufp.sd.minesweeper.client.minefield;

import edu.ufp.sd.minesweeper.client.minefield.jgui.MainFrame;
import edu.ufp.sd.minesweeper.client.minefield.jgui.MineSweeperMenuBar;
import edu.ufp.sd.minesweeper.client.models.GameModeManager;
import edu.ufp.sd.minesweeper.client.timers.SystemTimeUpdater;

/**
 * The main class. This class starts the application.
 *
 * @author Sorin ( soriniulus@yahoo.com ) At: Apr 8, 2007, 8:23:57 PM
 */
public class Driver {

    public static final String absPathToResourceIcons = "/edu/ufp/sd/minesweeper/client/resources/icons";

    public static void main(String[] args) {
        MainFrame.getInstance().startNewGame();
        MainFrame.getInstance().setLocationRelativeTo(null);
        MainFrame.getInstance().setJMenuBar(MineSweeperMenuBar.getInstance());
        GameModeManager.getInstance().addGameModeListener(MainFrame.getInstance());
        SystemTimeUpdater.getInstance();
        MainFrame.getInstance().pack();
    }
}
