package edu.ufp.sd.minesweeper.client.minefield.jgui;

import java.awt.Dimension;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.rmi.RemoteException;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.*;

import edu.ufp.sd.minesweeper.client.MinesweeperGameObserverImpl;
import edu.ufp.sd.minesweeper.client.MinesweeperGameObserverRI;
import edu.ufp.sd.minesweeper.client.minefield.listeners.BombButtonPressedListener;
import edu.ufp.sd.minesweeper.client.models.GameModeManager;
import edu.ufp.sd.minesweeper.server.MinesweeperGameSubjectRI;
import edu.ufp.sd.minesweeper.server.Session;
import edu.ufp.sd.minesweeper.server.State;

/**
 * This class represents a button in the minefield. When the button is pressed,
 * it will show up its state. If the button is an empty field, the buttons
 * closed to it will show their state automatically.
 * <p>
 * 
 * @author Sorin ( soriniulus@yahoo.com ) At: Apr 8, 2007, 12:50:57 PM
 */
public class BombButton extends JButton implements BombButtonPressedListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private int row;

	private int column;

	private int state;

	private Vector listenerList;

	private boolean buttonPressed;

	private int rightCliksNumber;


	private MinesweeperGameObserverRI gameObserverRI;

	/**
	 * This constructor builds a bomb button.
	 * <p>
	 * 
	 * @param row
	 *            the row index of the bomb button.
	 * @param column
	 *            the column index of the bomb button.
	 * @param state
	 *            the state of the button.
	 */
	public BombButton(int row, int column, int state, MinesweeperGameObserverRI gameObserverRI) {
		super();

		this.gameObserverRI = gameObserverRI;

		this.row = row;
		this.column = column;
		this.state = state;
		rightCliksNumber = 0;
		//
		setDefaultState();
		//
		addMouseListener(new BombButtonListener());
		//
		listenerList = new Vector();
		buttonPressed = false;
	}

	/**
	 * Adds a listener at this button.
	 * <p>
	 * 
	 * @param listener
	 *            a neighbour button that listens for this button's discovering.
	 */
	public void addBombButtonPressedListener(BombButtonPressedListener listener) {
		listenerList.add(listener);
	}

	/**
	 * Remove a listener of this button.
	 * 
	 * @param listener
	 *            a neighbour button that listens for this button's discovering.
	 */
	public void removeBombButtonPressedListener(
			BombButtonPressedListener listener) {
		listenerList.remove(listener);
	}

	private void setDefaultState() {
		setIcon(null);
		setPreferredSize(new Dimension(20, 20));
	}

	/**
	 * Returns the state of the button;
	 * <p>
	 * 
	 * @return the state of the button;
	 */
	public int getState() {
		return state;
	}

	/**
	 * Returns the row of this buttons.
	 * <p>
	 * 
	 * @return the row of this buttons.
	 */
	public int getRow() {

		return row;
	}

	/**
	 * Returns the column of this button.
	 * <p>
	 * 
	 * @return the column of this button.
	 */
	public int getColumn() {
		return column;
	}

	public void bombButtonPressed(String nickWhoPlayed) {
		buttonPressed = true;

		//System.out.println(getState());
		switch (getState()) {
		case -1:
		{
			Logger.getLogger(this.getClass().getName()).log(Level.INFO, "---------> Bomb Button !! <---------");
			setIcon(ImageIconResourcer.getInstance().getIcon_1());
			askForNewGame();
			fireBombButtonPressed();
			try {
				if(nickWhoPlayed != null && nickWhoPlayed.equals(gameObserverRI.getPlayer().getNickname())) {
					gameObserverRI.getSubjectRI().setPoints(gameObserverRI.getPlayer().getNickname(), -10);
				}
			} catch (RemoteException e) {
				e.printStackTrace();
			}
			return;
		}
		case 0: {
			setIcon(ImageIconResourcer.getInstance().getIcon0());
		}
			break;
		case 1: {
			setIcon(ImageIconResourcer.getInstance().getIcon1());
		}
			break;
		case 2: {
			setIcon(ImageIconResourcer.getInstance().getIcon2());
		}
			break;
		case 3: {
			setIcon(ImageIconResourcer.getInstance().getIcon3());
		}
			break;
		case 4: {
			setIcon(ImageIconResourcer.getInstance().getIcon4());
		}
			break;
		case 5: {
			setIcon(ImageIconResourcer.getInstance().getIcon5());
		}
			break;
		case 6: {
			setIcon(ImageIconResourcer.getInstance().getIcon6());
		}
			break;
		case 7: {
			setIcon(ImageIconResourcer.getInstance().getIcon7());
		}
			break;
		case 8: {
			setIcon(ImageIconResourcer.getInstance().getIcon8());
		}
			break;
		}
		MainFrame.getInstance().increaseNumberOfButtonsCorrectlyRevealed();
		fireBombButtonPressed();
		try {
			if(nickWhoPlayed != null && nickWhoPlayed.equals(gameObserverRI.getPlayer().getNickname())) {
				gameObserverRI.getSubjectRI().setPoints(gameObserverRI.getPlayer().getNickname(), 1);
			}
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Reveal the state of the button.
	 * 
	 */
	public void revealButton() {
		//Logger.getLogger(this.getClass().getName()).log(Level.INFO, "reveal Button pressed");
		switch (getState()) {
		case -1: {
			setIcon(ImageIconResourcer.getInstance().getIconMark());
		}
			break;
		case 0: {
			setIcon(ImageIconResourcer.getInstance().getIcon0());
		}
			break;
		case 1: {
			setIcon(ImageIconResourcer.getInstance().getIcon1());
		}
			break;
		case 2: {
			setIcon(ImageIconResourcer.getInstance().getIcon2());
		}
			break;
		case 3: {
			setIcon(ImageIconResourcer.getInstance().getIcon3());
		}
			break;
		case 4: {
			setIcon(ImageIconResourcer.getInstance().getIcon4());
		}
			break;
		case 5: {
			setIcon(ImageIconResourcer.getInstance().getIcon5());
		}
			break;
		case 6: {
			setIcon(ImageIconResourcer.getInstance().getIcon6());
		}
			break;
		case 7: {
			setIcon(ImageIconResourcer.getInstance().getIcon7());
		}
			break;
		case 8: {
			setIcon(ImageIconResourcer.getInstance().getIcon8());
		}
			break;
		}
	}

	private void askForNewGame() {
		MainFrame.getInstance().gameLost(row, column);
	}

	/**
	 * This class represents the listener for the bomb button.
	 * 
	 * @author Sorin ( soriniulus@yahoo.com ) At: Apr 8, 2007, 1:05:14 PM
	 */
	private class BombButtonListener implements MouseListener {

		public void mouseClicked(MouseEvent e) {

		}

		public void mouseEntered(MouseEvent e) {

		}

		public void mouseExited(MouseEvent e) {

		}

		public void mousePressed(MouseEvent e)
		{

			if (MainFrame.getInstance().isGameLost())
				return;

			if (MainFrame.getInstance().isGameWon())
				return;

			if (!MainFrame.getInstance().isGameStarted()) {
				JOptionPane.showMessageDialog(null, "Jogo ainda não começou. Mínimo 2 jogadores...");
				//MainFrame.getInstance().gameStarted(true); <- Jogo só começa após indicação do servidor (Minimo de 2P)
				return;
			}

			if (e.getModifiersEx() == MouseEvent.BUTTON1_DOWN_MASK
					+ MouseEvent.BUTTON3_DOWN_MASK) {

				if (getState() == getNumberOfMarkedField()) {
					fireMarkedFieldPressed();
				} else {
					fireIncorrectNumberOfMarkedFieldPressed(true);
				}

			} else if (e.getButton() == MouseEvent.BUTTON1) {

				if (BombButton.this.getIcon() != null)
					return;
				//===============================================================

				//bombButtonPressed();
				//fireBombButtonPressed();
				//O servidor é que é responsável por fazer clique na GUI de todos os "Observers".
				//==============================================

				// Enviar informação sobre clique.
				try {
					System.out.println(gameObserverRI.getPlayer().getNickname());
					// Check if it is players turn.
					if(gameObserverRI.getSubjectRI().isPlayerTurn(gameObserverRI.getPlayer().getNickname())) {
						State newState = new State();
						newState.action = "CLICK";
						newState.x = getRow();
						newState.y = getColumn();
						newState.playerContext = gameObserverRI.getPlayer().getNickname();
						gameObserverRI.getSubjectRI().setState(newState);
					}
					else {
						JOptionPane.showMessageDialog(null, "Não é a sua vez...");
					}
				} catch (RemoteException e1) {
					e1.printStackTrace();
				}

				//===============================================================


			} else if (e.getButton() == MouseEvent.BUTTON3) {

				if (!GameModeManager.getInstance().isQuestionsMarksOn()) {
					rightCliksNumber = 0;
					if (getIcon() == null) {
						setIcon(ImageIconResourcer.getInstance().getIconMark());
						GameModeManager.getInstance()
								.setBombsNumber(
										GameModeManager.getInstance()
												.getBombsNumber() - 1);
					} else if (!buttonPressed) {
						if (getIcon() != ImageIconResourcer.getInstance()
								.getIconQuestionMark()) {
							GameModeManager.getInstance().setBombsNumber(
									GameModeManager.getInstance()
											.getBombsNumber() + 1);
						}
						setIcon(null);
					}
				} else {
					rightCliksNumber++;
					if (rightCliksNumber == 3)
						rightCliksNumber = 0;
					switch (rightCliksNumber) {
					case 0: {
						setIcon(null);
					}
						break;
					case 1: {
						if (getIcon() == ImageIconResourcer.getInstance()
								.getIconMark()) {
							rightCliksNumber++;
							setIcon(ImageIconResourcer.getInstance()
									.getIconQuestionMark());
							GameModeManager.getInstance().setBombsNumber(
									GameModeManager.getInstance()
											.getBombsNumber() + 1);
						} else if (getIcon() == null) {
							setIcon(ImageIconResourcer.getInstance()
									.getIconMark());
							GameModeManager.getInstance().setBombsNumber(
									GameModeManager.getInstance()
											.getBombsNumber() - 1);
						}
					}
						break;
					case 2: {
						setIcon(ImageIconResourcer.getInstance()
								.getIconQuestionMark());
						GameModeManager.getInstance()
								.setBombsNumber(
										GameModeManager.getInstance()
												.getBombsNumber() + 1);
					}
						break;
					}
				}
			}
		}

		public void mouseReleased(MouseEvent e) {
			fireIncorrectNumberOfMarkedFieldPressed(false);
		}
	}

	/**
	 * Inform this button's listeners that this button was pressed.
	 */
	public void fireBombButtonPressed() {


		Logger.getLogger(this.getClass().getName()).log(Level.INFO, "fireBombButtonPressed() ");

		if (listenerList == null || listenerList.size() == 0)
			return;
		Object[] listeners = listenerList.toArray();
		if (getState() == 0)
		{
			for (int i = 0; i < listeners.length; i++)
			{
				if (((BombButton) listeners[i]).getState() != -1
						&& ((BombButton) listeners[i]).getIcon() == null)
				{
					((BombButton) listeners[i]).bombButtonPressed(null);

				}
			}
		}
	}

	/**
	 * This method is called when a field for which the bombs are supposed to be
	 * correctly set is 2-clicked.
	 * 
	 */
	private void fireMarkedFieldPressed()
	{
		Logger.getLogger(this.getClass().getName()).log(Level.INFO, "fireMarkedFieldPressed() ");

		if (listenerList == null || listenerList.size() == 0)
			return;
		Object[] listeners = listenerList.toArray();
		for (int i = 0; i < listeners.length; i++) {
			if (((BombButton) listeners[i]).getIcon() == null) {
				((BombButton) listeners[i]).bombButtonPressed(null);
			}
		}
	}

	private void fireIncorrectNumberOfMarkedFieldPressed(boolean pressed)
	{
		if (listenerList == null || listenerList.size() == 0)
			return;
		Object[] listeners = listenerList.toArray();
		for (int i = 0; i < listeners.length; i++) {
			if (pressed)
			{
				if (((BombButton) listeners[i]).getIcon() == null) {
					((BombButton) listeners[i]).setIcon(ImageIconResourcer
							.getInstance().getTryToRevealIcon());
				}
			} else {
				if (((BombButton) listeners[i]).getIcon() == ImageIconResourcer
						.getInstance().getTryToRevealIcon()) {
					((BombButton) listeners[i]).setIcon(null);
				}
			}
		}
	}

	/**
	 * Returns the number of marked field for this button.
	 * <p>
	 * 
	 * @return the number of marked field for this button.
	 */
	private int getNumberOfMarkedField() {
		if (listenerList == null || listenerList.size() == 0)
			return 0;
		Object[] listeners = listenerList.toArray();
		//
		int numberOfMarkedField = 0;
		//
		for (int i = 0; i < listeners.length; i++) {
			if (((BombButton) listeners[i]).getIcon() == ImageIconResourcer
					.getInstance().getIconMark()) {
				numberOfMarkedField++;
			}
		}
		//
		return numberOfMarkedField;
	}
}
