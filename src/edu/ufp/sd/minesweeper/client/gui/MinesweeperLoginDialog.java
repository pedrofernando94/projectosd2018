package edu.ufp.sd.minesweeper.client.gui;

import edu.ufp.sd.minesweeper.client.MinesweeperClient;
import edu.ufp.sd.minesweeper.server.MinesweeperPlayerHallRI;
import edu.ufp.sd.minesweeper.server.MinesweeperSessionFactoryRI;
import edu.ufp.sd.minesweeper.server.Session;

import javax.swing.*;
import java.awt.event.*;
import java.rmi.RemoteException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MinesweeperLoginDialog extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField nicknameTextField;
    private JLabel nicknameLabel;

    private MinesweeperSessionFactoryRI sessionFactoryRI;

    public MinesweeperLoginDialog(MinesweeperSessionFactoryRI sessionFactoryRI) {
        this.sessionFactoryRI = sessionFactoryRI;

        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        // Efectua Login
        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    onOK();
                } catch (RemoteException e1) {
                    e1.printStackTrace();
                }
            }
        });

        // Fecha a janela e o cliente
        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    }

    private void onOK() throws RemoteException {
        String nickname = nicknameTextField.getText();
        if( !nickname.isEmpty() ) {
            Session playerSession = sessionFactoryRI.loginOrRegister(nickname);
            if(playerSession != null) {
                MinesweeperClientHall clientHall = new MinesweeperClientHall(playerSession);
                clientHall.pack();
                clientHall.setVisible(true);
                //Logger.getLogger(MinesweeperClient.class.getName()).log(Level.INFO, "Conseguiu RI");
                dispose();
            }
        }
        else {
            JOptionPane.showMessageDialog(null, "Tem de preencher o campo nickname.");
        }
    }

    private void onCancel() {
        dispose();
    }

}
