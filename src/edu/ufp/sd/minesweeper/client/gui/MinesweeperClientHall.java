package edu.ufp.sd.minesweeper.client.gui;

import edu.ufp.sd.minesweeper.client.MinesweeperGameObserverImpl;
import edu.ufp.sd.minesweeper.client.MinesweeperGameObserverRI;
import edu.ufp.sd.minesweeper.client.minefield.jgui.MainFrame;
import edu.ufp.sd.minesweeper.client.minefield.jgui.MineSweeperMenuBar;
import edu.ufp.sd.minesweeper.client.models.GameModeManager;
import edu.ufp.sd.minesweeper.client.timers.SystemTimeUpdater;
import edu.ufp.sd.minesweeper.server.*;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

public class MinesweeperClientHall extends JFrame {
    private JList gamesList;
    private JButton createGameButton;
    private JButton joinGameButton;
    private JLabel gamesListLabel;
    private JPanel HallPanel;
    private JButton refreshGamesListButton;


    private Session playerSession;

    private DefaultListModel<String> gamesListModel = new DefaultListModel<>();;
    private ArrayList<MinesweeperGameSubjectRI> games;

    public MinesweeperClientHall(Session playerSession) throws RemoteException {
        super("Lobby");
        this.playerSession = playerSession;

        setContentPane(this.HallPanel);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        this.games = this.playerSession.getPlayerHallRI().listGames();
        updateGamesList(games);

        gamesList.setModel(gamesListModel);

        // Run when "Refresh" is pressed
        refreshGamesListButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    updateGamesList(playerSession.getPlayerHallRI().listGames());
                } catch (RemoteException e1) {
                    e1.printStackTrace();
                }
            }
        });

        // Run when "Create Game" is pressed
        createGameButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    updateGamesList(playerSession.getPlayerHallRI().createGame());
                } catch (RemoteException e1) {
                    e1.printStackTrace();
                }
            }
        });

        // Run when "Join Game" is pressed
        joinGameButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                // TODO: Should we save the observerRI in a the Session object?
                MinesweeperGameSubjectRI gameSubjectRI = games.get(gamesList.getSelectedIndex());
                try {
                    if(gameSubjectRI.canEnterGame()) {

                        MinesweeperGameObserverRI gameObserverRI = new MinesweeperGameObserverImpl(gameSubjectRI, playerSession);
                        MainFrame.getInstance().attachRemoteInterfaces(gameObserverRI);
                        MainFrame.getInstance().startNewGame();
                        MainFrame.getInstance().setLocationRelativeTo(null);

                        MineSweeperMenuBar.getInstance().attachRemoteInterfaces(gameObserverRI);
                        MainFrame.getInstance().setJMenuBar(MineSweeperMenuBar.getInstance());
                        GameModeManager.getInstance().addGameModeListener(MainFrame.getInstance());
                        SystemTimeUpdater.getInstance();

                        ArrayList<State> history = gameObserverRI.getSubjectRI().getHistory();

                        if (history.size() > 0) {
                            for (State button : history) {
                                button.action = "CLICK_HIST";

                                System.out.println("History : " + button.x + " " + button.y);
                                gameObserverRI.update(button);
                            }
                        }
                        MainFrame.getInstance().pack();
                    }
                    else {
                        JOptionPane.showMessageDialog(null, "Este jogo está cheio. Máximo de 4 jogadores foi atingido...");
                    }

                } catch (RemoteException e1) {
                    e1.printStackTrace();
                }


            }
        });
    }

    private void updateGamesList(ArrayList<MinesweeperGameSubjectRI> games) throws RemoteException {
        this.games = games;
        gamesListModel.clear();

        // TODO: Change this so we have named games, maybe.
        int i = 0;
        for (MinesweeperGameSubjectRI gameSubjectRI : games) {
            gamesListModel.addElement("Game #" + ++i);
        }
    }

}
