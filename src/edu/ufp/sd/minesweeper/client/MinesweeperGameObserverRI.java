package edu.ufp.sd.minesweeper.client;

import edu.ufp.sd.minesweeper.server.MinesweeperGameSubjectRI;
import edu.ufp.sd.minesweeper.server.Session;
import edu.ufp.sd.minesweeper.server.State;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.HashMap;

public interface MinesweeperGameObserverRI extends Remote {

    public void update(State state) throws RemoteException;

    public MinesweeperGameSubjectRI getSubjectRI() throws RemoteException;
    public Session getPlayer() throws RemoteException;
    public void updatePoints(HashMap<String, Integer> points) throws RemoteException;

}
